# README #

NinerNav README

### What is this repository for? ###

This repository houses the Android project for Capstone Group NINER Nav. NinerNav, our android application, will help new students, visitors, and those who are generally unfamiliar with UNCC's campus navigate their way around.

### What can NinerNav do?###

NinerNav will allow users to view a map of the UNCC campus with all building names included. There will be a search bar at the top of the map where users can type in the name of any academic or residential building on campus. Buildings can also be clicked while browsing the map. Once there is a pinpoint above your desired destination, click the pinpoint and then click the "Navigate To ..." button that pops up to draw a route from your location to the building. 

Additionally, users can go to the "Navigate" screen from the main menu and enter a to and from location in order to get the quickest walking route from the first location to the second. 

Once navigation mode begins, the map will zoom in on the user's location and follow them until they click the cancel button (ideally, when they find their destination).

### How do I get set up? ###

To get NinerNav running on your android device, first clone the repository. Once you have the files, open the project in Android Studio. Once the project has loaded, run the APK on either a virtual machine or load it straight to your connected android device via USB


### Who do I talk to? ###

If you have any questions about NinerNav, contact one of the team members

 - Chris McNutt
 - Kartik Mistry
 - Daniel Ramskill
 - Yiding Wang
 - Kartik Patel